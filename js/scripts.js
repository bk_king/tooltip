(function () {

    function init(elements) {

        appendEvent(elements);
    }
    var globalTooltip = null,
            objectSize = {};

    function appendEvent(elements) {
        var i, length = elements.length;
        for (i = 0; i < length; i++) {
            elements[i].addEventListener('mouseenter', showTooltip, false);
            elements[i].addEventListener('mouseleave', hideTooltip, false);
        }
    }
    function showTooltip(e) {
        var target = e.target;
        objectSize = {
            x: target.offsetLeft,
            y: target.offsetTop,
            width: target.offsetWidth
        };
        createTooltip(target.title);
        target.removeAttribute("title");
    }
    function setTolltipPosition() {
        globalTooltip.style.left = (objectSize.x + objectSize.width / 2) - globalTooltip.offsetWidth / 2 + 'px';
        globalTooltip.style.top = objectSize.y - globalTooltip.offsetHeight - 10 + 'px';
    }
    function hideTooltip(e) {
        var target = e.target;
        globalTooltip.parentNode.removeChild(globalTooltip);
        target.setAttribute('title', globalTooltip.textContent);

    }
    function createTooltip(title) {
        var tooltip = document.createElement('div');
        tooltip.className = 'tooltip';
        tooltip.textContent = title;
        document.body.appendChild(tooltip);
        globalTooltip = tooltip;
        setTolltipPosition();


    }
    window.t00ltip = init;
})();